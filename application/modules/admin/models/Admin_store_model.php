<?php

class Admin_store_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getAdminStore()
    {
        $query = $this->db->get('stores');
        //$this->db->last_query(); 
        return $query->result_array();
    }

  public function changeStoreStatus($codeId, $toStatus)
    {
        $this->db->where('id', $codeId);
        if (!$this->db->update('stores', array(
                    'is_approved' => $toStatus
                ))) {
            log_message('error', print_r($this->db->error(), true));
            show_error(lang('database_error'));
        }
    }

}
