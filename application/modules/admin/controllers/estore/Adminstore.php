<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Adminstore extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_store_model');
    }

    public function index()
    {
        // echo "1"; die;
        $this->login_check();
        /*if (isset($_GET['delete'])) {
            $this->Admin_users_model->deleteAdminUser($_GET['delete']);
            $this->session->set_flashdata('result_delete', 'User is deleted!');
            redirect('admin/adminusers');
        }
        if (isset($_GET['edit']) && !isset($_POST['username'])) {
            $_POST = $this->Admin_users_model->getAdminUsers($_GET['edit']);
        }*/

         if (isset($_GET['tostatus']) && isset($_GET['codeid'])) {
        $this->Admin_store_model->changeStoreStatus($_GET['codeid'], $_GET['tostatus']);
            redirect('admin/estore');
        }


        $data = array();
        $head = array();
        $head['title'] = 'Administration - eStore';
        $head['description'] = '!';
        $head['keywords'] = '';
        $data['stores'] = $this->Admin_store_model->getAdminStore();

        //print_r($data['store']); die;


      /*  $this->form_validation->set_rules('username', 'User', 'trim|required');
        if (isset($_POST['edit']) && $_POST['edit'] == 0) {
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
        }
        if ($this->form_validation->run($this)) {
            $this->Admin_users_model->setAdminUser($_POST);
            $this->saveHistory('Create admin user - ' . $_POST['username']);
            redirect('admin/adminusers');
        }*/

        $this->load->view('_parts/header', $head);
        $this->load->view('store/adminStore', $data);
        $this->load->view('_parts/footer');
        $this->saveHistory('Go to Admin eStore');
    }

}
